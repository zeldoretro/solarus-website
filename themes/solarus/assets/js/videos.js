'use strict';

document.addEventListener('DOMContentLoaded', () => {
  // Get all buttons and iframes.
  const buttons = document.querySelectorAll(`[id^=video-button]`);
  if (buttons.length == 0) return;
  const iframes = document.querySelectorAll(`[id^=video-iframe]`);
  if (iframes.length != buttons.length) return;
  const thumbnails = document.querySelectorAll(`[id^=video-thumbnail]`);
  if (thumbnails.length != buttons.length) return;

  function getCorrespondigElement(elements, videoId, htmlId) {
    const regexp = new RegExp(`${htmlId}-(.+)`);
    for (const element of elements) {
      const elementId = element.id;
      const elementMatch = elementId.match(regexp);
      if (elementMatch.length >= 2) {
        const elementYoutubeVideoId = elementMatch[1];
        if (elementYoutubeVideoId === videoId) {
          return element;
        }
      }
    }
    return null;
  }

  // Associate buttons, thumbnails and iframes.
  const videos = [];
  for (const button of buttons) {
    const buttonId = button.id;
    const buttonMatch = buttonId.match(/video-button-(.+)/);
    if (buttonMatch.length >= 2) {
      const videoId = buttonMatch[1];
      const video = {
        videoId: videoId,
        button: button,
        thumbnail: getCorrespondigElement(thumbnails, videoId, 'video-thumbnail'),
        iframe: getCorrespondigElement(iframes, videoId, 'video-iframe'),
      };
      videos.push(video);
    }
  }

  // Add YouTube scripts to the page.
  let youtubeScriptsAdded = false;
  function ensureYoutubeScriptsAdded() {
    if (youtubeScriptsAdded) return;

    const tag = document.createElement('script');
    tag.id = 'iframe-demo';
    tag.src = 'https://www.youtube.com/iframe_api';
    const firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    youtubeScriptsAdded = true;
  }

  window.onYouTubeIframeAPIReady = () => {
    for (const video of videos) {
      video.player = new YT.Player(`video-iframe-${video.videoId}`, {
        height: '640',
        width: '360',
        videoId: video.videoId,
        playerVars: {
          playsinline: 1,
        },
        events: {
          onReady: () => {
            if (video.shouldPlay) {
              video.shouldPlay = false;
              video.player.playVideo();
            }
          },
        },
      });
    }
  };

  // Register to click events, to show the video.
  for (const video of videos) {
    video.button.onclick = () => {
      ensureYoutubeScriptsAdded();
      video.shouldPlay = true;

      // Hide button and thumbnail.
      if (video.button) {
        video.button.style.setProperty('display', 'none', 'important');
      }
      if (video.thumbnail) {
        video.thumbnail.style.setProperty('display', 'none', 'important');
      }
      // Show Youtube video.
      if (video.iframe) {
        video.iframe.style.setProperty('display', 'block', 'important');
      }
    };
  }
});
