'use strict';

const MIN_SEARCH_TEXT_LENGTH = 2; // chars count

const SECTION_SCORES = {
  empty: 10,
  about: 10,
  page: 10,
  games: 1,
  news: 0.25,
};

const IGNORED_WORDS = {
  en: ['A', 'AN', 'THE', 'OF', 'IN', 'ON', 'OUT', 'FROM'],
  fr: [
    'A',
    'AN',
    'THE',
    'OF',
    'IN',
    'ON',
    'OUT',
    'FROM',
    'UN',
    'UNE',
    'LE',
    'LA',
    'LES',
    'DE',
    'DU',
    'DES',
    'DANS',
  ],
};

function htmlDecode(input) {
  const doc = new DOMParser().parseFromString(input, 'text/html');
  return doc.documentElement.textContent;
}

function isInsideElement(event, element) {
  const rect = element.getBoundingClientRect();
  const x = event.clientX;
  if (x < rect.left || x >= rect.right) return false;
  const y = event.clientY;
  if (y < rect.top || y >= rect.bottom) return false;
  return true;
}

function prepareQuery(query) {
  query = query.trim();
  query = query.replace(/ +(?= )/g, '');
  return query.toUpperCase();
}

function getTokens(query, languageCode) {
  return query.split(' ').filter((word) => !IGNORED_WORDS[languageCode].includes(word));
}

function getPageScore(queryUpperCase, upperCaseWords, item) {
  // Prepare tokens.
  const title = item.title ? item.title : '';
  const excerpt = item.excerpt ? item.excerpt : '';
  const section = item.section && item.section.length > 0 ? item.section : 'empty';
  const tags = item.tags ? item.tags.map((tag) => tag.toUpperCase()) : [];
  const titleUpperCase = title.toUpperCase();
  const excerptUpperCase = excerpt.toUpperCase();

  let score = 0;

  // First, check if the exact query is matched if there are multiple words.
  if (upperCaseWords.length > 1) {
    const exactWordsInOrderInTitle = titleUpperCase.includes(queryUpperCase);
    const exactWordsInOrderInExcerpt = excerptUpperCase.includes(queryUpperCase);
    if (exactWordsInOrderInTitle && exactWordsInOrderInExcerpt) score += 4;
    else {
      if (exactWordsInOrderInTitle) score += 2;
      if (exactWordsInOrderInExcerpt) score += 1;
    }
  }

  // Then, check if some of the words are matched, in any order.
  upperCaseWords.forEach((upperCaseWord) => {
    const includedInTitle = titleUpperCase.includes(upperCaseWord);
    const includedInExcerpt = excerptUpperCase.includes(upperCaseWord);

    if (includedInTitle && includedInExcerpt) score += 1;
    else if (includedInTitle) score += 0.75;
    else if (includedInExcerpt) score += 0.5;
  });

  // Use the tags.
  if (tags.length > 0) {
    tags.forEach((tag) => {
      if (queryUpperCase.includes(tag) || tag.includes(queryUpperCase)) {
        score += 3;
      }
    });
  }

  // Use the section scores.
  if (score > 0 && section in SECTION_SCORES) {
    score += SECTION_SCORES[section];
  }

  return {
    title: title,
    excerpt: excerpt,
    score: score,
    relPermalink: item.relPermalink,
  };
}

function getSearchResult(query, list, languageCode) {
  if (list === null || list === undefined || !Array.isArray(list) || list.length === 0) return [];

  // Don't search for queries that are too short to be meaningful.
  if (query.length < MIN_SEARCH_TEXT_LENGTH) return [];

  // Make sure the query is valid.
  const queryUpperCase = prepareQuery(query);

  // Prepare tokens.
  const upperCaseWords = getTokens(queryUpperCase, languageCode);

  // Get items scores.
  const scores = list.map((item) => getPageScore(queryUpperCase, upperCaseWords, item));

  // Keep only items that matched.
  const items = scores.filter((item) => {
    return item.score > 0;
  });

  // Sort items by matching score.
  const sortedItems = items.sort((item1, item2) => {
    // Sort best scores first.
    if (item1.score < item2.score) return 1;
    if (item1.score > item2.score) return -1;
    // Sort same scores alphabetically.
    if (item1.title < item2.title) return -1;
    if (item1.title > item2.title) return 1;
    return 0;
  });

  // Get only the first n items.
  const result = sortedItems.slice(0, 10);

  return result;
}

function getBaseURL() {
  if (IS_GITLAB_PAGES) {
    const pathElements = window.location.pathname.split('/');
    return `${window.location.origin}/${pathElements.slice(1, 2).join('/')}`;
  } else {
    return window.location.origin;
  }
}

function getCurrentLanguageCode() {
  const pathElements = window.location.pathname.split('/');
  const languageCode = IS_GITLAB_PAGES ? pathElements[2] : pathElements[1];
  return languageCode.length === 2 ? languageCode.toLowerCase() : DEFAULT_LANGUAGE;
}

function getJsonIndexUrl(languageCode, jsonPath) {
  const languagePath = languageCode === DEFAULT_LANGUAGE ? '' : `${languageCode}/`;
  const baseURL = getBaseURL();
  return `${baseURL}/${languagePath}${jsonPath}`;
}

function fetchJsonIndex(url, onDataReceived) {
  const startTime = performance.now();
  fetch(url)
    .then((response) => response.json())
    .then((data) => {
      console.info(
        `JSON index fetched at ${url} in ${(performance.now() - startTime).toFixed(2)}ms.`
      );
      onDataReceived(data.data);
    })
    .catch((error) => console.error(`Failed to fetch JSON index at ${url}: ${error.message}`));
}

function makeGlobalSearchResultItem(result, resultIndex, setCurrentIndex) {
  const elementLi = document.createElement('li');
  elementLi.classList.add('search-result');

  const elementA = document.createElement('a');
  elementA.classList.add('search-result-link');
  elementA.href = getBaseURL() + result.relPermalink;
  elementLi.appendChild(elementA);

  elementA.addEventListener('mousedown', () => {
    elementA.focus();
  });

  elementA.addEventListener('focusin', () => {
    setCurrentIndex(resultIndex);
  });

  const elementTitle = document.createElement('div');
  elementTitle.classList.add('search-result-title');
  elementA.appendChild(elementTitle);
  elementTitle.innerText = htmlDecode(result.title);

  const elementExcerpt = document.createElement('div');
  elementExcerpt.classList.add('search-result-excerpt');
  elementA.appendChild(elementExcerpt);
  elementExcerpt.innerText = htmlDecode(result.excerpt);

  const elementUrl = document.createElement('div');
  elementUrl.classList.add('search-result-url');
  elementA.appendChild(elementUrl);
  elementUrl.innerText = result.relPermalink;

  return elementLi;
}

// ----------------------------------------------------------------------------

const registerGlobalSearchbox = () => {
  const bodyElement = document.getElementsByTagName('body')[0];
  const searchbox = document.getElementById('searchbox');
  const searchPopup = document.getElementById('searchPopup');
  const searchResults = document.getElementById('searchResults');
  const searchNoResults = document.getElementById('searchNoResults');
  const languageCode = getCurrentLanguageCode();
  const indexUrl = getJsonIndexUrl(languageCode, 'index.json');

  let fetchedList = null;
  let searchText = '';
  let currentIndex = undefined;

  // Utilities

  const updatePopupPosition = () => {
    const searchboxRect = searchbox.getBoundingClientRect();
    const searchPopupRect = searchPopup.getBoundingClientRect();
    const x = searchboxRect.left - (searchPopupRect.width - searchboxRect.width);
    searchPopup.style.left = `${x}px`;
  };

  const openModal = () => {
    searchPopup.style.display = 'block';
    updatePopupPosition();
  };

  const closeModal = () => {
    searchPopup.style.display = 'none';
  };

  const ensurePagesIndexFetched = () => {
    if (fetchedList !== null) return;

    setIndexLoadingVisible(true);

    fetchJsonIndex(indexUrl, (data) => {
      fetchedList = data;
      setIndexLoadingVisible(false);
      // Trigger a search if the user typed before the JSON had been downloaded.
      triggerSearch();
    });
  };

  const triggerSearch = () => {
    setCurrentIndex(undefined);
    const result = getSearchResult(searchText, fetchedList, languageCode);
    populateListElement(result);
  };

  const setIndexLoadingVisible = (visible) => {
    const element = document.getElementById('searchLoadingIndex');
    element.style.display = visible ? 'flex' : 'none';
  };

  const populateListElement = (results) => {
    // Clear current content.
    while (searchResults.firstChild) {
      searchResults.removeChild(searchResults.firstChild);
    }

    // Add new content.
    if (results.length > 0) {
      searchResults.style.display = 'block';
      searchNoResults.style.display = 'none';

      results.forEach((result, resultIndex) => {
        const elementLi = makeGlobalSearchResultItem(result, resultIndex, setCurrentIndex);
        searchResults.appendChild(elementLi);
      });
    } else {
      searchResults.style.display = 'none';
      searchNoResults.style.display = fetchedList === null ? 'none' : 'block';
    }
  };

  const setCurrentIndex = (index) => {
    if (index === undefined) {
      currentIndex = undefined;

      const len = searchResults.children.length;
      for (let i = 0; i < len; i++) {
        searchResults.children[i].classList.remove('current');
      }
    } else if (typeof index === 'number') {
      currentIndex = Math.max(0, Math.min(index, searchResults.children.length - 1));

      const len = searchResults.children.length;
      for (let i = 0; i < len; i++) {
        const child = searchResults.children[i];

        if (i === currentIndex) {
          child.classList.add('current');
          child.scrollIntoView({
            behavior: 'smooth',
            block: 'nearest',
            inline: 'nearest',
          });
        } else {
          child.classList.remove('current');
        }
      }
    }
  };

  // Events on body.

  bodyElement.addEventListener('mousedown', (event) => {
    if (searchPopup.style.display !== 'block') return;

    if (!isInsideElement(event, searchPopup) && !isInsideElement(event, searchbox)) {
      closeModal();
    }
  });

  bodyElement.addEventListener('keydown', (event) => {
    if (event.key === 'Escape') {
      closeModal();
      event.preventDefault();
      return;
    }
  });

  window.addEventListener('resize', () => {
    updatePopupPosition();
  });

  // Events on searchbox.

  searchbox.addEventListener('input', () => {
    if (searchbox.value != searchText) {
      searchText = searchbox.value;
      triggerSearch();
    }

    if (searchText.length > MIN_SEARCH_TEXT_LENGTH) {
      openModal();
    } else {
      closeModal();
    }
  });

  searchbox.addEventListener('focusin', () => {
    ensurePagesIndexFetched();
    if (searchText.length > MIN_SEARCH_TEXT_LENGTH) {
      openModal();
    }
  });

  searchbox.addEventListener('focusout', (event) => {
    const focusedElement = event.relatedTarget;
    if (!searchPopup.contains(focusedElement)) {
      closeModal();
    }
  });

  searchbox.addEventListener('keydown', (event) => {
    if (searchText.length > MIN_SEARCH_TEXT_LENGTH) {
      openModal();
    }

    if (event.key === 'ArrowDown') {
      // Go to next element.
      event.preventDefault();
      setCurrentIndex(currentIndex !== undefined ? currentIndex + 1 : 0);
    } else if (event.key === 'ArrowUp') {
      // Go to previous element.
      event.preventDefault();
      setCurrentIndex(currentIndex !== undefined ? currentIndex - 1 : 0);
    } else if (event.key === 'Enter') {
      // Go to link href.
      event.preventDefault();
      const currentItem =
        currentIndex !== undefined ? searchResults.children.item(currentIndex) : null;
      if (currentItem) {
        const linkElement = currentItem.firstElementChild;
        window.location.assign(linkElement.href);
      } else {
        window.location.assign(`/${languageCode !== 'en' ? languageCode + '/' : ''}search`);
      }
    }
  });

  // Events on popup.
  searchPopup.addEventListener('keydown', (event) => {
    if (event.key === 'ArrowDown') {
      // Go to next element.
      event.preventDefault();
      setCurrentIndex(currentIndex !== undefined ? currentIndex + 1 : 0);
    } else if (event.key === 'ArrowUp') {
      // Go to previous element.
      event.preventDefault();
      setCurrentIndex(currentIndex !== undefined ? currentIndex - 1 : 0);
    }
  });
};

const registerGameSearchbox = () => {
  const searchbox = document.getElementById('game-searchbox');
  if (!searchbox) return;
  const listNode = document.getElementById('game-list');
  if (!listNode) return;

  const languageCode = getCurrentLanguageCode();
  const indexUrl = getJsonIndexUrl(languageCode, 'games/index.json');

  let fetchedList = null;
  let searchText = '';

  const ensureIndexFetched = () => {
    if (fetchedList !== null) return;

    fetchJsonIndex(indexUrl, (data) => {
      fetchedList = data;
      // Trigger a search if the user typed before the JSON had been downloaded.
      triggerSearch();
    });
  };

  const getMatchingItems = (query, list, languageCode) => {
    // Make sure the query is valid.
    const queryUpperCase = prepareQuery(query);

    // Don't search for queries that are too short to be meaningful.
    if (query.length < MIN_SEARCH_TEXT_LENGTH) return list;

    // Prepare tokens.
    const upperCaseWords = getTokens(queryUpperCase, languageCode);

    // Filter games.
    const matchingItems = list.filter((item) => {
      if (item.title && item.title.toUpperCase().includes(queryUpperCase)) return true;

      const developerValue = item.developer
        ? Array.isArray(item.developer)
          ? item.developer.join(' ').toUpperCase()
          : item.developer.toUpperCase()
        : '';
      if (developerValue.includes(queryUpperCase)) return true;

      for (const upperCaseWord of upperCaseWords) {
        if (item.title && item.title.toUpperCase().includes(upperCaseWord)) return true;
        if (developerValue.includes(upperCaseWord)) return true;
      }

      return false;
    });

    return matchingItems;
  };

  const updateList = (matchingItems) => {
    // Hide/show list elements based on the result.
    const listElements = listNode.getElementsByTagName('li');
    for (const listElement of listElements) {
      const listElementId = listElement.dataset.searchid;
      const found = matchingItems.find((item) => {
        return item.id === listElementId;
      });
      listElement.style.display = found ? 'block' : 'none';
    }
  };

  const triggerSearch = () => {
    const matchingItems = getMatchingItems(searchText, fetchedList, languageCode);
    updateList(matchingItems);
  };

  searchbox.addEventListener('focusin', () => {
    ensureIndexFetched();
  });

  searchbox.addEventListener('input', () => {
    if (searchbox.value != searchText) {
      searchText = searchbox.value;
      triggerSearch();
    }
  });
};

const registerNewsSearchbox = () => {
  const searchbox = document.getElementById('news-searchbox');
  if (!searchbox) return;
  const listNode = document.getElementById('news-list');
  if (!listNode) return;

  const languageCode = getCurrentLanguageCode();
  const indexUrl = getJsonIndexUrl(languageCode, 'news/index.json');

  let fetchedList = null;
  let searchText = '';

  const ensureIndexFetched = () => {
    if (fetchedList !== null) return;

    fetchJsonIndex(indexUrl, (data) => {
      fetchedList = data;
      // Trigger a search if the user typed before the JSON had been downloaded.
      triggerSearch();
    });
  };

  const getMatchingItems = (query, list, languageCode) => {
    // Make sure the query is valid.
    const queryUpperCase = prepareQuery(query);

    // Don't search for queries that are too short to be meaningful.
    if (query.length < MIN_SEARCH_TEXT_LENGTH) return list;

    // Prepare tokens.
    const upperCaseWords = getTokens(queryUpperCase, languageCode);

    // Filter games.
    const matchingItems = list.filter((item) => {
      if (item.title && item.title.toUpperCase().includes(queryUpperCase)) return true;
      if (item.excerpt && item.excerpt.toUpperCase().includes(queryUpperCase)) return true;

      for (const upperCaseWord of upperCaseWords) {
        if (item.title && item.title.toUpperCase().includes(upperCaseWord)) return true;
        if (item.excerpt && item.excerpt.toUpperCase().includes(upperCaseWord)) return true;
      }

      return false;
    });

    return matchingItems;
  };

  const updateList = (matchingItems) => {
    // Hide/show list elements based on the result.
    const listElements = listNode.getElementsByTagName('li');
    for (const listElement of listElements) {
      const listElementId = listElement.dataset.searchid;
      const found = matchingItems.find((item) => {
        return item.permalink === listElementId;
      });
      listElement.style.display = found ? 'block' : 'none';
    }
  };

  const triggerSearch = () => {
    const matchingItems = getMatchingItems(searchText, fetchedList, languageCode);
    updateList(matchingItems);
  };

  searchbox.addEventListener('focusin', () => {
    ensureIndexFetched();
  });

  searchbox.addEventListener('input', () => {
    if (searchbox.value != searchText) {
      searchText = searchbox.value;
      triggerSearch();
    }
  });
};

const registerSearchPageSearchbox = () => {
  const searchbox = document.getElementById('searchpage-searchbox');
  if (!searchbox) return;
  const listNode = document.getElementById('searchpage-list');
  if (!listNode) return;
  const noResultsNode = document.getElementById('searchpage-noresults');
  if (!noResultsNode) return;

  const languageCode = getCurrentLanguageCode();
  const indexUrl = getJsonIndexUrl(languageCode, 'index.json');

  let fetchedList = null;
  let searchText = '';

  const setIndexLoadingVisible = (visible) => {
    const element = document.getElementById('searchpage-loading');
    element.style.display = visible ? 'block' : 'none';
  };

  const ensureIndexFetched = () => {
    if (fetchedList !== null) return;

    setIndexLoadingVisible(true);

    fetchJsonIndex(indexUrl, (data) => {
      fetchedList = data;
      setIndexLoadingVisible(false);
      // Trigger a search if the user typed before the JSON had been downloaded.
      triggerSearch();
    });
  };

  const triggerSearch = () => {
    const result = getSearchResult(searchText, fetchedList, languageCode);
    populateListElement(result);
  };

  const populateListElement = (results) => {
    // Clear current content.
    while (listNode.firstChild) {
      listNode.removeChild(listNode.firstChild);
    }

    // Add new content.
    if (results.length > 0) {
      listNode.style.display = 'flex';
      noResultsNode.style.display = 'none';

      results.forEach((result, resultIndex) => {
        const elementLi = makeGlobalSearchResultItem(result, resultIndex);
        listNode.appendChild(elementLi);
      });
    } else {
      listNode.style.display = 'none';
      noResultsNode.style.display =
        fetchedList === null || searchText.length < MIN_SEARCH_TEXT_LENGTH ? 'none' : 'block';
    }
  };

  searchbox.addEventListener('focusin', () => {
    ensureIndexFetched();
  });

  searchbox.addEventListener('input', () => {
    if (searchbox.value != searchText) {
      searchText = searchbox.value;
      triggerSearch();
    }
  });
};

document.addEventListener('DOMContentLoaded', function () {
  registerGlobalSearchbox();
  registerGameSearchbox();
  registerNewsSearchbox();
  registerSearchPageSearchbox();
});
