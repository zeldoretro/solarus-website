---
title: Contribuer
excerpt: Comment contribuer au projet de moteur de jeu Solarus.
tags: [contribuer, contribution, code source, donation, don, aide, bug, traduction, traduire]
aliases:
  - /fr/development/how-to-contribute
---

## Moteur

Tous le source code des projets Solarus se trouve sur Gitlab. Le moteur est fait en C++. Il est recommandé d'être compétent dans ce langage et de maîtriser la programmation de jeu vidéo avant d'être capable de contribuer. L'éditeur de quête _Solarus Quest Editor_ et le lanceur de quêtes _Solarus Launcher_ sont faits en C++ avec Qt (et QML pour le lanceur).

- [Projets Solarus sur Gitlab]({{< param "gitlab" >}})
- [Documentation du code C++ du moteur](https://www.solarus-games.org/developer_doc/latest)

### Modifier le code source

Si vous voulez modifier le code source de n'importe quel projet :

1. Clonez le dépôt pour lequel vous voulez contribuer.
2. Créez votre propre branche et faites vos modifications dans cette branche.
3. Créez une Merge Request.

### Nous faire part d'un bug ou d'une demande de fonctionnalité

Si vous voulez nous informer d'un bug, ou si vous avez une demande de nouvelle fonctionnalité:

1. Regardez si quelqu'un n'a pas déjà créé un ticket sur la page Gitlab du projet.
2. Créez votre ticket, et soyez aussi précis que possible. Joignez des captures d'écran si besoin.

## Site web

Le site web est fait avec [Hugo](https://gohugo.io). Il utilise un thème personnalisé fait sur-mesure pour répondre à nos besoins.

### Ajouter un jeu au site web

Si le développement de votre jeu est fini, ou suffisamment avancé, vous pouvez l'ajouter à la page [Jeux](https://www.solarus-games.org/fr/games) de ce site, qui recense les jeux faits avec Solarus.

Créez une _Merge Request_ quand votre branche est prête à être intégrée, et nous vérifierons l'éligibilité de votre jeu ainsi que la validité des fichiers ajoutés.

## Art

Si vous avez la fibre artistique, nous avons besoin d'aide dans le département pixel-art. Toute contribution de tileset, font ou sprite sont les bienvenues ! Vous voulez peut-être également aider pour le design du site Internet ou des outils ?

- [Éléments de design graphqiue pour Solarus sur Gitlab](https://gitlab.com/solarus-games/solarus-design)
- [Pack de ressources libres pour Solarus sur Gitlab](https://gitlab.com/solarus-games/solarus-free-resource-pack)

## Traduction

### Éditeur de jeu et lanceur de jeu

L'éditeur et le lanceur de quête ont besoin d'être traduits dans toutes les langues possibles ! N'hésitez pas à ajouter votre propre langue. Puisqu'ils sont faits avec Qt, vous devriez utiliser le logiciel de Qt fait pour cela, appelé Qt Linguist.

- [Documentation sur la traduction d'une application Qt](https://doc.qt.io/qt-5/qtlinguist-index.html).

### Jeux

Les jeux peuvent également bénéficier de nouvelles traductions. Solarus Quest Editor inclut un système de traduction pour vous simplifier le travail.

- [Documentation sur la traduction de quête](https://www.solarus-games.org/doc/latest/translation.html).

### Site web

Nous n'avons pas prévu de traduire le site web en d'autres langages que Français et Anglais pour le moment.

## Donations

Les développeurs de Solarus réalisent ce moteur sur leur temps libre. Si vous appréciez notre travail, vos dons pourront contribuer aux frais d'héberger et nous montrer que Solarus est apprécié.

- [Faire un don](/fr/about/donate)
