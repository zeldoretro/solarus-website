---
title: Legal Terms
excerpt: Legal information (legal form, licenses) about the Solarus project.
tags: [legal, law, ip, intellectual property, licensing, license, npo, non profit, nonprofit, organization, association]
aliases:
  - /legal
  - /nonprofit-organization
  - /about/nonprofit-organization
  - /en/about/nonprofit-organization
---

## Solarus Labs Legal Form

![Solarus Labs logo](solarus-labs-logo.png)

**Solarus Labs** is a **French nonprofit organization**, ruled by the law of the 1st of July 1901 (according to French law), created in March 2021. The goal of this organization is the development, the promotion and the education of free software for video game creation.

Solarus Labs, like any organization, is a legal person having its own legal capacity, and its responsibility, distinct from the persons directing it.

Volunteering is the rule concerning the organization: all donations and receipts are kept and used by the legal person, i.e. the organization, without any distribution to the directors or the members, despite their work and their investment.

All donations will go directly to the organization, and will be reinvested into the project.

### Documents

Most of the administrative documents are publicly available in the organization's [Gitlab repository](https://gitlab.com/solarus-games/solarus-labs).

There is a yearly meeting during which the organization members meet and take stock of the past year's finances and organization development.

### Board

The current board of Solarus Labs is composed by:

- **President:** Christophe Thiéry
- **Treasurer:** Kévin Baumann
- **Vice-Treasurer:** Benjamin Schweitzer
- **Secretary:** Olivier Cléro

## Applicable Law

The Solarus Labs organization is an organization under **French law**. It is based in France and exercises most of the management of its activity in this country. The applicable law concerning it (as well as its software and services) is therefore in all cases French law, as provided for in articles 14 and 15 of the Civil Code.

![French Government logo](french_gouv_logo.png)

## Licenses and Copyright

### Software

Software offered by Solarus Labs is all **free and open source software**, under the license [GNU GPL v3](https://www.gnu.org/licenses/quick-guide-gplv3.html). All of the software source codes are accessible on [Gitlab](https://gitlab.com/solarus-games).

This license allows all uses of the software and their redistribution in the same form or after modification, provided that these modified software comply with the conditions of the original license, in particular the publication of source code and the authorization of redistribution.

![GNU GPL v3 logo](gpl_v3_logo.png)

### Resources

Resources produced by Solarus Labs are all **free**, under license [Creative Commons Attribution-ShareAlike 4.0 International 4.0](https://creativecommons.org/licenses/by-sa/4.0/). The resources include all types of content produced as part of the Solarus project: images, sounds, texts, content from this website, etc.

This does not mean that these resources are in the public domain, though. This license allows the use, sharing and modification, provided that the modified resources comply with the conditions of the original license, in particular the credit of the author of the original work.

![CC-BY-SA](cc_by_sa.png)

### Intellectual Property

Subsidiary to the conditions of the license, the common French law of intellectual property applies, in particular for copyright. Any change of license or redistribution under other conditions is prohibited. Likewise, the origin of software, even partially reused, must always be visible to users.

## Patents

According to article `L 611-10` of the Intellectual Property Code, software patents are not allowed under French law. At Community level, Article `52` of the European Patent Convention also excluded the protection of computer programs.

Therefore, software supplied by Solarus Labs is not liable for licenses on any software patent, regardless of its origin.

## Responsibility for use of software

The Solarus Labs association declines all responsibility for the illegal use of its software.
