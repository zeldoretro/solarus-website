---
date: '2012-01-16'
excerpt: Alors que Zelda Mystery of Solarus DX vient de dépasser les 10000 téléchargements en moins d'un mois (merci à tous ! :D), l'émission Star Player...
tags:
- solarus
title: Zelda Solarus DX à la télé
---

Alors que [Zelda Mystery of Solarus DX](http://www.zelda-solarus.com/jeu-zsdx-download) vient de dépasser les 10000 téléchargements en moins d'un mois (merci à tous ! :D), l'émission Star Player vient de parler de notre jeu dans son dernier numéro !

Voyez plutôt (ça commence à 2 mn 00) :

Star Player est une émission de jeux vidéo sur Direct Star (chaîne de la TNT) et présentée par Bertrand Amar que je remercie pour cette belle reconnaissance ^\_^
