---
date: '2004-11-04'
excerpt: 'Vous savez peut-être (ou peut-être pas) que notre création Zelda : Mystery of Solarus, bien avant d''être informatisée, existait déjà presque...'
tags:
- solarus
title: Zelda Solarus version papier !
---

Vous savez peut-être (ou peut-être pas) que notre création Zelda : Mystery of Solarus, bien avant d'être informatisée, existait déjà presque entièrement sur papier. Et oui car c'est un projet qui date de 1995 !

Pour la première fois, nous vous proposons des images de cette version papier.

![](niveau7_mini.jpg)

[Voir les images de la version papier](http://www.zelda-solarus.com/jeux.php?jeu=zs&zone=versionpapier)
