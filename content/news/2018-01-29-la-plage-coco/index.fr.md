---
date: '2018-01-29'
excerpt: Cette semaine, pour le projet A Link to the Dream, je vous présente la plage Coco qui se trouve au sud du village des Mouettes, pas loin du donjon...
tags:
  - solarus
title: La plage Coco
---

Cette semaine, pour le projet A Link to the Dream, je vous présente la **plage Coco** qui se trouve au sud du village des Mouettes, pas loin du donjon 1: la cave Flagello. Sachez que cette partie de la carte **est presque terminée**.

![](1-300x240.png)

![](2-300x240.png)

Comme d'habitude, n'hésitez pas à donner votre avis sur ces deux images. En tout cas, celle de gauche fait particulièrement rêver !
