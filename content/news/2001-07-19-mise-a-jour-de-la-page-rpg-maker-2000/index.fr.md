---
date: '2001-07-19'
excerpt: 'Vous vous demandez comment je peux faire pour programmer un jeu tel que Zelda : Mystery of Solarus ? Et plus généralement comment fonctionne RPG...'
tags:
- solarus
title: Mise à jour de la page RPG Maker 2000
---

Vous vous demandez comment je peux faire pour programmer un jeu tel que Zelda : Mystery of Solarus ? Et plus généralement comment fonctionne RPG Maker 2000 ? Comme promis, je viens de mettre à jour la page du logiciel.

Vous pouvez donc satisfaire votre curiosité en visitant cette page. Vous y trouverez une description de l'utilisation de RPG Maker 2000, screenshots à l'appui. Comment dessine-t-on des niveaux, comment programme-t-on les événements... Les réponses sont [ici](http://www.zelda-solarus.com/rm2k.php3).
