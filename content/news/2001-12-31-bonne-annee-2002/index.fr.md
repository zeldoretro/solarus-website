---
date: '2001-12-31'
excerpt: Nous tenons à vous souhaiter une très bonne et heureuse année 2002 ! Nous souhaitons que vous soyez toujours fidèles à notre création qui...
tags:
- solarus
title: Bonne Année 2002
---

Nous tenons à vous souhaiter une très bonne et heureuse année 2002 ! Nous souhaitons que vous soyez toujours fidèles à notre création qui s'annonce fort probablement pour le mois de Mars/Avril. Nous pouvons vous dire d'ores et déjà que le donjon 8 est fini au niveau de la conception et que la programmation va bientôt commencer.

Merci de continuer à visiter ce site, de nous encourager, de participer à ce site en y ajoutant vos messages sur le forum !

Merci de vos nombreux téléchargements de la démo du jeu, merci de votre soutient, de vos remarques, de vos suggestions, de vos réactions et...

VIVE ZELDA !
