---
date: '2001-12-16'
excerpt: Voici une musique inédite de Zelda Solarus ! Dans votre aventure, vous aurez à explorer la montagne des terreurs dont voici la musique en qualité...
tags:
- solarus
title: Cool Music
---

Voici une musique inédite de Zelda Solarus ! Dans votre aventure, vous aurez à explorer la montagne des terreurs dont voici la musique en qualité CD.

Télécharger (2,34 Mo) - 44100 Hz, 128Kbits/s, Stéréo

(la musique a été supprimée)
