---
date: '2002-05-01'
excerpt: Nous avons ouvert la rubrique Fan Arts du site après avoir reçu de très belles pochettes de DriK. Merci DriK, tu viens de lancer une nouvelle...
tags:
- solarus
title: Fan Arts ! A vos pinceaux !
---

Nous avons ouvert la rubrique Fan Arts du site après avoir reçu de très belles pochettes de DriK. Merci DriK, tu viens de lancer une nouvelle rubrique du site !

Vous pouvez vous aussi nous envoyer vos bôô dessins par e-mail, rendez-vous alors sur la page Fan Arts de la section SITE

[Voir les FAN ARTS](http://www.zelda-solarus.com/fans.php)
