---
date: '2010-12-18'
excerpt: La démo du projet Zelda Mystery of Solarus DX est sortie il y a tout juste un an aujourd'hui. J'ai pensé que c'était l'occasion de vous donner des...
tags:
  - solarus
title: 'Zelda Solarus DX : un an après la démo'
---

La démo du projet Zelda Mystery of Solarus DX est sortie il y a tout juste un an aujourd'hui. J'ai pensé que c'était l'occasion de vous donner des nouvelles, mais aussi de marquer un tournant dans le développement.

Les nouvelles se font rares en ce moment à propos du projet, il faut l'admettre. La raison principale est toute simple : comme je l'expliquais dans la mise à jour précédente, je me suis lancé depuis la fin du mois d'août dans de (très) grosses modifications du moteur, au détriment de l'avancement de la quête. Ces modifications visent à permettre à qui le souhaite de créer des jeux facilement avec le moteur, sans être contraint d'utiliser exactement les mêmes objets et ennemis que dans Zelda Mystery of Solarus. Pour clarifier les choses, rappelons que le projet se divise en fait en trois projets :

- 1. La quête (Zelda Mystery of Solarus DX) : il s'agit de l'ensemble des sprites, des musiques, des sons, des maps, des dialogues, des objets de l'équipement, des énigmes et des évènements du scénario.

- 2. Le moteur de jeu (Solarus), c'est le fichier exécutable qui lance une quête donnée. Le [blog de développement du moteur](http://www.solarus-engine.org) (en anglais) donne des informations détaillées sur son fonctionnement.

- 3. L'éditeur de quête, qui donne la possibilité à ceux que cela intéresse de créer leur propre quête (c'est-à-dire leur propre jeu). C'est l'évolution de l'éditeur de maps que j'utilise jusqu'à présent pour Zelda Mystery of Solarus DX.

Initialement (par exemple dans la démo), le moteur n'était conçu que pour savoir lancer Zelda: Mystery of Solarus DX. Ou en tout cas, si vous vouliez créer une autre quête, vous pouviez changer les maps, les graphismes et les sons, mais absolument pas la liste des objets de l'équipement et de l'inventaire. Il fallait modifier vous-même le code C++ du moteur pour cela. Depuis le mois d'août donc, j'ai beaucoup travaillé sur le moteur pour le rendre plus indépendant de la quête. L'objectif de tout cela est de rendre le moteur réutilisable. Et c'est bien parti : tous les objets de l'équipement et de l'inventaire sont maintenant décidés par la quête et non imposés par le moteur ! Les personnes intéressées par les améliorations du moteur peuvent consulter le [blog de développement](http://www.solarus-engine.org) ou même suivre la liste des [changements](https://github.com/christopho/solarus/commits/master) apportés presque quotidiennement au code source (tout est public).

Concernant le point 1 (la quête), je ne vous cache pas qu'à cause du point 2, elle n'a pas du tout avancé pendant tout ce temps. Ces modifications ne sont pas terminées, de toute façon il y aura toujours des choses à faire. Dans la quête, j'en suis toujours au donjon 2, qui est fini depuis un bon moment, mais sans ennemis ni boss pour l'instant. Pour le point 3 (l'éditeur de quêtes), je peux vous annoncer qu'à la suite du recrutement de la mise à jour précédente, 19oj19 a rejoint l'équipe pour se consacrer à l'amélioration de l'éditeur. Il a déjà réalisé des belles choses dont je vous parlerai dans une autre mise à jour :).

J'ai décidé de faire une pause dans les améliorations du moteur. À partir d'aujourd'hui, je prévois d'avancer le plus possible sur la quête, c'est-à-dire de créer les maps de la suite du jeu. Si tout va bien, vous aurez donc droit à des nouvelles captures d'écran dans un avenir proche ^\_^. Je ne prévois pas de faire une nouvelle démo car la première est suffisamment stable malgré quelques bugs et quelques fonctionnalités en plus dans la version de développement actuelle. L'équipe se concentre ainsi sur le jeu complet uniquement, en espérant pouvoir envisager de penser à une date, un jour :P
