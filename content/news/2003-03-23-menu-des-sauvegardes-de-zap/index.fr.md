---
date: '2003-03-23'
excerpt: 'Ca faisait un certain temps que nous ne vous avions pas donné de screenshots de notre prochaine création : Zelda Advanced Project (titre...'
tags:
- solarus
title: Menu des sauvegardes de ZAP
---

Ca faisait un certain temps que nous ne vous avions pas donné de screenshots de notre prochaine création : Zelda Advanced Project (titre provisoire), alors on se rattrape un peu aujourd'hui ! Voici un screenshot du menu de sélection des sauvegardes de la démo, que nous venons juste de finir :

![](fileselect.gif)

Comme vous pouvez le constater c'est autre chose qu'avec RPG Maker :-)

[Voir les autres screenshots](http://www.zelda-solarus.com/jeux.php?jeu=zf&zone=scr)
