---
date: '2019-01-25'
excerpt: A video advertisement made with (almost only) Solarus.
tags:
  - solarus
thumbnail: cover.jpg
title: Video Ad for Chase Bliss Audio
---

We are proud to announce that a video advertisement was made with (almost only) Solarus. It's an ad that parodies Zelda, and promotes Dark World, the new guitar pedal by Chase Bliss Audio. The pedal was released on November 19th, 2018.

## The video

{{< youtube id=ueyQWd7mAUA >}}

Credits:

- Video: [Olivier Cléro](https://twitter.com/olivclr)
- Music: [Gil Assayas](https://twitter.com/GLASYSmusic)

## The context

Joel Korte, [Chase Bliss Audio](https://chaseblissaudio.com) creator was looking for someone who could create a Zelda theme trailer for their new guitar pedal, Dark World, named in honour to the second world map of our beloved game _A Link to the Past_, the best SNES game by a huge margin, and maybe the best video game ever made too (at least for us at Solarus, but if you follow us, you may have noticed we really like this game more than anything, right ?).

Gil Assayas, who made the [beautiful trailer music](https://www.youtube.com/watch?v=pbKXKN5s6SI), contacted us first. Since I was free at the moment, I agreed to work on the project. We began to talk about this at the end of September. I was busy at the time, so I told Joel and Gil that I would begin to work on the video at the end of October. This would have given me 2 to 3 weeks, which would have been rather comfortable. However, the other projects I work on took me more time than planned, and eventually I had only 6-7 days to make to entire video! Very short, indeed. We lowered our ambitions without diluting the initial concept, and in the end I had just enough time to finish before the trailer release date!

## The execution

Since the time frame was very short, the quickest way to startup the project was duplicating our current project, _A Link to the Dream_, the incoming Solarus-made _Link's Awakening_ remake. The Solarus engine is well advanced and already allows you to create Action-RPG à la _A Link to the Past_, also called Zelda-like, _almost_ out of the box. So, with a complete and functional engine entirely dedicated to making Zelda-like games, all I had to do was making assets for a game, and record me playing it in a video. I used the development version of Solarus, which was 1.6 at the time, and not publicly available yet.

Gil Assayas had already made a beautiful rendition of the famous Dark World music theme from _A Link to the Past_. His music was used as the breadcrumb trail to build the scenes of the video. I began with the beginning of the video, then the end, then filled up until no more time was left.

What I had to do:

- Fork the project in Git
- Drawing pixel art for logos, the pedal, custom entities (Aseprite)
- Drawing Joel's character animated sprite (Aseprite)
- Scripting in Lua (code) the logo, title screen and other non-playable scenes (Solarus)
- Making maps (Solarus)
- Record everything into videos (OBS)
- Cut and edit (Adobe After Effects)

### The Gitlab project

I simply forked our current _Zelda_ project into my own repository. This is accessible on GitLab at the address [gitlab.com/oclero/darkworld](https://gitlab.com/oclero/darkworld), for those interested. The project already contains much code and assets. It was solid foundations to build upon. I only modified what was necessary. The game is playable. Just open it with Solarus.

![The game in Solarus Launcher](playable_game-1-300x214.png)

### Logos

I began with Chase Bliss Audio, and coded an animation a bit like the old Game Boy startup screen. It was quite easy to make.

![Chase Bliss logo](chasebliss_logo.png)

Next, I made the ending screen, with the Dark World logo, and a pixel art rendition of the guitar pedal. Drawing the pedal was rather hard and long: one day. Animating everything with Lua code is easier and quicker.

![Pedal sprite](pedal_sprite.png)

![Pedal logo](pedal_logo.png)

Next is the recreation of _A Link to the Past_ famous title screen. I looked for the original pictures of the background, and changed the colours to fit the Dark World colours. I was working at the time of a basic recreation of the _ALTTP_ logo with Solarus to add it to our _ALTTP_ resource pack, so it was the perfect time to finish the work. The animation of the water was made with a shader, a new feature in Solarus 1.6.

![Title screen](title_screen.png)

### Sprites

Joel wanted to avoid to use Link's sprite because, of course, copyright issues. So he sent me pictures of himself as a basis to create a little character that looks like him. Sprites in ALTTP are 24x24 pixels. Making a few pixels look like him was hard but I am quite proud of the result, very Nintendo-esque and cartoony.

Link's sprite has dozen of animation cycles (walking, swimming, fighting, etc.) so to save time, I only drew the necessary ones:

- Walking (left, top, right, bottom directions)
- Stopped (when the character is not moving)
- Brandishing (when the character brandishes the item he gets at the end)

Here is only the walking animation, frame by frame:

![Joel sprite walking animation](joel_walking.png)

### Maps

Solarus Quest Editor makes this an easy task, though quite long. Placing tiles is very simple and intuitive with or map editor. However, getting a nice looking map is harder since it has to look 'natural': i.e. no symmetries, no horizontal or vertical lines of trees, etc. It must not feel artificial. I created some custom tiles for the dungeon entrance: statues with guitars, guitar heads, amp stacks. Initially, I planned to made a more detailed entrance but time was running out.

![Dungeon full map](map_dungeon_out.png)

![Forest full map](map_forest.png)

### Recording screen

Simple: I just recorded myself playing the game. The game's resolution is the HD resolution (1920x1080 pixels) divided by 4, so I recorded only the game window, in the highest quality available. I could have written a script to make the hero automatically move, but it would have been longer to directly record myself. I made a few attempts to get the perfect take though.

### Editing

There is almost no editing since everything is done with Solarus: transitions, scenes, etc. I just used the video and enlarged it to HD and added Gil's marvellous music on it. A simpler app than After Effects would have been equivalent in this process, since no VFX are added.

## Conclusion

And that's all! Actually it is a full week of word, from 9am to 12pm, non-stop, to be in time. It was nice to work with people who share love for _The Legend of Zelda_ series.
