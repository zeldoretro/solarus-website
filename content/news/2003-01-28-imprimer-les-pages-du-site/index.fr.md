---
date: '2003-01-28'
excerpt: Jusqu'à présent, ce n'était pas très pratique d'imprimer les soluces et les autres pages du site, à cause du design. J'ai donc rajouté un lien...
tags:
- solarus
title: Imprimer les pages du site
---

Jusqu'à présent, ce n'était pas très pratique d'imprimer les soluces et les autres pages du site, à cause du design. J'ai donc rajouté un lien "Version imprimable" dans le menu de gauche. Il vous permettra d'afficher n'importe quelle page du site sur fond blanc, sans le design autour. L'idéal donc pour imprimer :-)
