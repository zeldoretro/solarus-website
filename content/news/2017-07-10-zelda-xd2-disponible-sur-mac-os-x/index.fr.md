---
date: '2017-07-10'
excerpt: 'Notre dernière création parodique Zelda XD2: Mercuris Chess est (enfin !) disponible sur Mac OS X ! Merci à Vlag qui a réalisé le...'
tags:
- solarus
title: Zelda XD2 disponible sur Mac OS X
---

Notre dernière création parodique Zelda XD2: Mercuris Chess est (enfin !) disponible sur Mac OS X ! Merci à Vlag qui a réalisé le portage.

- [Télécharger Zelda XD2 1.0.4](http://www.zelda-solarus.com/zs/article/zxd2-telechargements/)

J'en profite pour vous signer qu'une mise à jour a été effectuée récemment. Elle corrige des bugs et ajoute des nouveautés comme un accès au bureau des objets trouvés et quelques nouvelles blagues et références, mais je ne vous en dis pas plus pour ne rien dévoiler ! Et aussi un écran de statistiques à la fin du jeu, qui vous montre si vous avez fini le jeu à 100% ou non :

![stats](stats-300x225.png)

L'écran de statistiques affiche même le temps que vous avez mis pour finir le jeu. L'occasion rêver de tenter des speedrun :D

Et si vous n'avez pas encore testé le jeu, profitez-en avec cette dernière version 1.0.4 qui est vraiment aboutie. Ceux qui y ont joué se sont rapidement rendus compte que ce jeu n'est pas seulement une blague du premier avril, c'est aussi techniquement et graphiquement notre meilleur jeu complet jusqu'ici.

On cherche d'ailleurs des traducteurs pour réaliser la version anglaise du jeu. Si vous connaissez des volontaires, contactez-nous. On cherche si possible des gens dont l'anglais est la langue natale. C'est en tout cas un gros travail car il y a énormément de texte (plus que dans Zelda Mystery of Solarus XD tout entier) dont de nombreuses références.
