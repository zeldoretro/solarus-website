---
date: '2017-01-16'
excerpt: Comme promis, chaque semaine on va vous donner une capture d'écran de Zelda Mercuris' Chest par semaine (au moins !). Voici un travail en cours...
tags:
- solarus
title: 'Zelda Mercuris'' Chest : une image de donjon'
---

Comme promis, chaque semaine on va vous donner une capture d'écran de Zelda Mercuris' Chest par semaine (au moins !).

Voici un travail en cours par Metallizer sur l'un des donjons du jeu :

![zmc_sacred_crater_1](zmc_sacred_crater_1-300x225.png)

Tout ceci est en travaux et risque encore d'évoluer mais cela vous donne un petit aperçu ! Ce qui est sûr, c'est qu'il va y avoir de l'épique dans ce donjon.
