---
date: '2009-12-15'
excerpt: Alors que vous êtes peut-être en plein dans votre aventure dans Spirit Tracks, je me permets d'interrompre votre locomotive quelques instants pour...
tags:
  - solarus
title: J-3 !
---

Alors que vous êtes peut-être en plein dans votre aventure dans Spirit Tracks, je me permets d'interrompre votre locomotive quelques instants pour vous donner quelques informations concernant la sortie toute proche de la démo de [Zelda Mystery of Solarus DX](http://www.zelda-solarus.com/jeu-zsdx).

La finalisation de la démo se passe bien : tout - ou presque - est déjà prêt pour vendredi. Je peux désormais vous annoncer avec certitude quelles langues seront disponibles : français et allemand (finis à 100%), et sauf imprévu de dernière minute, anglais (terminé mais en cours de correction). Les traducteurs pourront vous confirmer que leur travail demande énormément de temps et que c'est loin d'être une tâche facile. Bravo donc à eux pour leur investissement ^^. D'autres langues sont prévues (néerlandais et espagnol pour l'instant) mais arriveront un peu plus tard. Si vous êtes bilingue, n'hésitez pas à nous contacter si vous souhaitez traduire la démo (et le jeu complet) dans une langue supplémentaire :).

Au niveau des systèmes supportés, comme annoncé précédemment, le jeu tourne sous Windows, Linux, Mac OS X et Amiga OS 4. Là aussi, bravo aux développeurs pour qui ça n'a pas toujours été facile de faire tout fonctionner correctement ^^. Les sources seront disponibles en même temps que le jeu, donc on pourra encore agrandir la liste. Le moteur de jeu est sous licence GPL.

Pour ne rien vous cacher, étant donné que la démo est prête depuis un moment, j'avance dans le jeu complet et même plutôt bien vu que je vais très bientôt attaquer le deuxième donjon ^\_^.

Voilà, en attendant, plus que 3 jours avant de jouer à la démo, sachant qu'avec Spirit Tracks on a largement de quoi s'occuper :P.

**Mise à jour** : finalement, la traduction en néerlandais sera bien disponible elle aussi, et dès le 18 décembre !
