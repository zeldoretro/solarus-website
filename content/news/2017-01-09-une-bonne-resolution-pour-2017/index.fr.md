---
date: '2017-01-09'
excerpt: Avant toute chose, toute l'équipe vous souhaite une bonne et heureuse année 2017 ! 2016 a été pour la communauté de création de jeux...
tags:
- solarus
title: Une bonne résolution pour 2017
---

Avant toute chose, toute l'équipe vous souhaite une bonne et heureuse année 2017 !

2016 a été pour la communauté de création de jeux Solarus une année très importante avec la sortie de Solarus 1.5. Mais en 2017, on vous promet moins d'avancées dans Solarus pour vraiment mettre l'accélérateur sur nos jeux en cours de développement.

Avec bien sûr le principal d'entre eux, Zelda Mercuris' Chest, dont voici trois nouvelles captures d'écran. Merci à Newlink pour son aide précieuse !

![chateau](chateau-300x224.png)
![desert_1](desert_1.png)
![lac](lac-300x225.png)

Et puis comme bonne résolution pour cette nouvelle année, vous montrer une nouvelle capture d'écran chaque semaine ! On s'y engage !
