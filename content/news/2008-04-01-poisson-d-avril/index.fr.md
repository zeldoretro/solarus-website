---
date: '2008-04-01'
excerpt: Comme certains d'entre vous l'ont soupçonné, toute cette annonce n'était qu'un vaste canular.
tags:
  - solarus
title: Poisson d'avril !!!
---

Comme certains d'entre vous l'ont soupçonné, toute cette annonce n'était qu'un vaste canular :D
Afin de clarifier la situation, je vais résumer les évènements, car il s'est passé des choses sur le forum. Tout a commencé hier soir, lorsqu'un certain **Christofaux** s'est inscrit. Aujourd'hui il a présenté son soi-disant projet (Zelda Solarus DX donc) sur le forum, puis j'en ai parlé à mon tour de manière officielle sur le site, dans la précédente mise-à-jour. Certains se sont vite doutés du pot-aux-roses : ils ont deviné que Christofaux n'était autre que moi-même ! Autrement dit, **Christofaux était en fait le vrai.**

Le projet Zelda Solarus DX était bien sûr un coup monté de longue date, et j'aurais eu du mal à vous faire croire de son existence beaucoup plus longtemps :P
En effet, plusieurs indices montraient bien que le projet était inventé de toutes pièces :

- Les captures d'écran étaient très basiques : les deux écrans d'intérieur étaient simplement repris de Mystery of Solarus (avec peu de changements).

- L'écran des sauvegardes vient tout droit de Mercuris' Chest. Seul l'écran-titre est vraiment nouveau, c'est Metallizer qui l'a fait pour l'occasion :)

- Certains l'ont fait remarquer très justement : 400 heures de travail, ce n'était pas très réaliste sachant que Mercuris' Chest a été annulé par manque de temps. Et 400 heures de travail pour aboutir à ces simples captures d'écrans, c'était louche ! :lol:

- J'ai une copine que j'aime (^\_^), j'ai un travail et je n'ai plus du tout le temps de me consacrer à un projet d'une telle ampleur.

Le projet de ce poisson d'avril remonte à quelques mois en arrière. Personne de l'équipe n'était au courant, pas même l'équipe du site et les modérateurs du forum, et pas même ceux qui ont cru contribuer au (faux) projet Zelda Solarus DX. Désolé à ceux à qui j'ai fait croire à l'existence de ce projet il y a longtemps, mais ça faisait partie du canular. En particulier, désolé à Metallizer à qui j'ai demandé de faire l'écran-titre pour rien. Désolé à Vaatiking01 à qui j'avais demandé de faire des sprites. Désolé à George à qui j'avais demandé de composer des musiques. Désolé à Geomaster avec qui nous avons beaucoup discuté du gameplay pour rien. Et désolé à Noxneo à qui j'avais demandé de développer un faux éditeur de maps :D
L'annonce aurait été moins crédible sans tout ça.

Bref, après cette journée bien animée, le site va reprendre son activité normale :) Sur ce bonne nuit à tous, et surtout, restez sur vos gardes le 1er avril !
