---
date: '2001-08-20'
excerpt: 'Une des grandes innovations de Zelda : Mystery of Solarus : le nouveau menu. Pour commencer, voici deux screenshots : Comme vous pouvez le...'
tags:
  - solarus
title: Des infos sur le nouveau menu
---

Une des grandes innovations de Zelda : Mystery of Solarus : le nouveau menu. Pour commencer, voici deux screenshots :

[](solarus-ecran16.png)

[](solarus-ecran23.png)

Comme vous pouvez le voir, le menu est inspiré de Zelda 3. Zelda : Mystery of Solarus est (à ma connaissance) le premier jeu à ne pas utiliser le menu texte par défaut de RPG Maker 2000 (je parle bien sûr du jeu final et non pas de la [démo](http://www.zelda-solarus.com/demo.php3)).

Lorsque vous appuyez sur la touche Echap, le menu graphique révolutionnaire apparaît. Ensuite, vous contrôlez un curseur pour sélectionner "Retour au jeu", "Sauvegarder", et "Quitter". Vous pouvez aussi choisir un objet, de la même manière que dans A Link to the Past. La principale différence est que l'objet que vous sélectionnez est utilisé directement lorsque vous appuyez sur Espace. Par exemple, lorsque vous sélectionnez les Bottes de Pégase et que vous appuyez sur Espace, le menu disparaît et Link se met à foncer comme un dingue. Ensuite, vous pouvez réutiliser les Bottes de Pégase ou utiliser un autre objet en revenant au menu.

Dans le menu, sont également affichés vos Coeurs, vos Rubis, vos Fragments de Coeur, votre Equipement, le nombre d'enfants sauvés, les objets de donjon si nécessaire, et même le temps de jeu !

Voilà, vous savez tout...
