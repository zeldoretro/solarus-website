---
date: '2022-04-01'
excerpt: We have an update to share about the launch timing of this much anticipated game.
tags:
- solarus
- games
thumbnail: cover.png
title: 'Release date for The Legend of Zelda: Mercuris'' Chest'
---

Solarus creator and *The Legend of Zelda: Mystery of Solarus* producer, Christopho, has an update to share about the launch timing of *The Legend of #Zelda: Mercuris' Chest*. Please take a look.

{{< youtube "bHj5qEnq0fA" >}}
