---
date: '2014-08-03'
excerpt: Vous êtes de plus en plus nombreux à utiliser Solarus pour créer votre propre jeu et nous vous en remercions. Une nouvelle version du moteur vient...
tags:
- solarus
title: Solarus 1.2.1 disponible, nouveaux tutos en vidéo
---

Vous êtes de plus en plus nombreux à utiliser Solarus pour créer votre propre jeu et nous vous en remercions. Une nouvelle version du moteur vient de sortir. Pas de nouveautés : il s'agit de corrections de bugs qui pouvaient vous gêner dans le développement de votre quête. Parmi ces problèmes, il y avait l'image du logo Solarus qui ne fonctionnait pas lorsque vous créez une quête avec l'éditeur. Et dans l'API Lua, quelques bugs ont été corrigés dans les nouveautés récentes, en particulier avec les entités custom et les séparateurs. Consultez la [liste des changements](https://github.com/christopho/solarus/blob/v1.2/ChangeLog) pour plus de détails.

- Télécharger [Solarus 1.2 + l'éditeur de quêtes](http://www.solarus-games.org/downloads/solarus/win32/solarus-1.2.0-win32.zip) pour Windows
- Télécharger le [code source](http://www.solarus-games.org/downloads/solarus/solarus-1.2.0-src.tar.gz)
- [Blog de développement Solarus](http://www.solarus-games.org/)

De plus, les tutoriels vidéo vont avoir droit à une remise à jour car beaucoup de choses ont changé depuis la version 1.0 avec laquelle les premiers d'entre eux avaient été faits. Dans le même temps, je les enregistre aussi en anglais ! L'occasion pour vous de découvrir ou de redécouvrir comment créer un jeu avec Solarus si cela vous tente. Le premier chapitre est déjà disponible, les suivants sont bien sûr en préparation !

- [Tutoriel vidéo en français](https://www.youtube.com/watch?v=DKLrGyMqvaU)
- [Tutoriel vidéo en anglais](http://youtu.be/T9mEFmRVlBQ)
