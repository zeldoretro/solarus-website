---
date: '2019-08-15'
excerpt: Fixing some regressions following the last update.
tags:
- solarus
thumbnail: cover.jpg
title: Bugfix release 1.6.2 for Solarus
---

Following the regressions caused by the recent version 1.6.1, here is another bugfix release. Solarus goes then to version **1.6.2** and teletransporters bugs are now fixed.

## Changelog

### Changes for Solarus 1.6.2

![Solarus logo](solarus_logo.png)

#### Engine changes

- Fix scrolling teletransporters with a square size (#1412).
- Fix hero displayed above stairs walls.

### Changes for Solarus Quest Editor 1.6.2

![Solarus Quest Editor logo](sqe_logo.png)

- Fix crash when closing tileset views (#467).
- Fix broken enemies in the initial quest (#466).
- Automatically install translation files.
- Update French translation.

### Changes for Zelda Mystery of Solarus DX 1.12.2

![Mystery of Solarus DX logo](mos_dx_logo.png)

- Fix some scrolling teletransporters broken in Solarus 1.6.1.

### Changes for Zelda Mystery of Solarus XD 1.12.2

![Mystery of Solarus XS logo](mos_xd_logo.png)

- Fix freaking cave teletransporters broken in 1.6.1.
