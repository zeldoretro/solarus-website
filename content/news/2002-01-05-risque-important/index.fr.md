---
date: '2002-01-05'
excerpt: Jusqu'à maintenant, ZS est hébergé sans trop de problèmes chez FREE parce que je possède un abonnement ainsi que c'est moi ait créé ce compte...
tags:
- solarus
title: Risque important
---

Jusqu'à maintenant, ZS est hébergé sans trop de problèmes chez FREE parce que je possède un abonnement ainsi que c'est moi ait créé ce compte nommé zeldasolarus !

Cet abonnement me fait payer la communication locale (14 cts/min) et les factures téléphonique deviennent très salées ! C'est pourquoi je me suis inscrit au forfait 50 hrs de Freetelecom pour 15?/mois (soit environ 98fr/mois) !

Quel est le problème ? Il est que je disposerai d'un nouveau login et je me demande si Free retirera l'ancien et par conséquent les comptes supplémentaires dont zeldasolarus.

Pas de panique ! J'ai pris la précaution de tout sauvegarder (base de données et pages) afin de se préparer à une éventuelle fermeture temporaire !

Si Free supprime ce compte, j'en créé un identique sous mon nouveau login principal et tout repart !

Voila ! Toutes les explications ont été données ! Si vous n'arrivez plus à acceder à ce site, pas de panique, cela veut dire que nous faisons notre nécéssaire pour le rétablir ! Cela ne sera que temporaire !
