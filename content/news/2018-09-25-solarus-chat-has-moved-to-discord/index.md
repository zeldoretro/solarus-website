---
date: '2018-09-25'
excerpt: ' We were previously using Gitter to chat. However, it has multiple issues so we decided to move to Discord. You can now join the server to chat...'
tags:
- solarus
title: Solarus Chat has moved to Discord
---

![](Discord-LogoWordmark-Color-300x102.png)

We were previously using Gitter to chat. However, it has multiple issues so we decided to move to Discord. You can now [join the server](https://discord.gg/yYHjJHt) to chat with us, ask questions, and help development.
