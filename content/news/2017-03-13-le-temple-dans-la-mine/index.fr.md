---
date: '2017-03-13'
excerpt: Ceux qui ont joué à la démo de Zelda Mercuris' Chest reconnaùtront sans doute les lieux ! Rappelons que le Temple du Rail, donjon qui...
tags:
- solarus
title: Le temple dans la mine
---

Ceux qui ont joué à la démo de Zelda Mercuris' Chest reconnaîtront sans doute les lieux !

![rail_temple_chest](rail_temple_chest-300x225.png)

Rappelons que le Temple du Rail, donjon qui était dans la démo, sera bien intégré dans le jeu complet mais remasterisé avec des décors inédits. Et quelques petites modifications car 4 trésors dans un seul donjon ça fait beaucoup !
