---
date: '2020-10-12'
excerpt: Le logo Solarus peut désormais être utilisé dans des projets closed-source.
tags:
- solarus
thumbnail: cover.jpg
title: Le logo Solarus est désormais sous license MIT/CC-BY
---

À côté du moteur (qui reste GPL), nous avons réalisé des jeux et du code Lua que nous avons publié sous licence **GPL**. Cependant, GPL n'est pas compatible avec des projets closed-source, donc il n'était pas autorisé d'utiliser les scripts Lua dans des projets commerciaux. La communauté a alors initié le pack de ressources [MIT Starter Quest](/fr/development/resource-packs/mit-starter-quest), compatible avec la licence MIT, et qui peut être utilisé dans une quête Solarus commerciale.

**Le logo Solarus était jusqu'alors sous licence GPL (code) et CC-BY-SA (sprite, sound)**, ce qui signifiait que l'utiliser dans un projet non-GPL rendait le proket également GPL, ce qui n'était pas voulu. Donc, en accord avec ses créateurs (Olivier Cléro pour le logo, Maxs pour le code, et Diarandor pour le son), nous avons décidé de changer sa licence.

**Il est désormais sous licence MIT (code) et CC-BY (sprite, sound)**, ce qui signifie que vous pouvez l'utiliser dans un projet commerial closed-source, tant que vous créditez les auteurs. Il sera inclus dans le MIT Starter Pack.

Cela concerne les fichiers :

- `solarus_logo.lua`
- `solarus_logo.png`
- `solarus_logo.ogg`

Si vous vous posez encore des questions sur les licences, vous trouverez de l'aide dans le [chapitre du tutoriel à propos des licences](/en/development/tutorials/solarus-official-guide/basics/choosing-a-license) (en anglais).
