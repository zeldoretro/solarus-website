---
date: '2010-04-01'
excerpt: Lorsque les ténèbres apparaissent, la lame d'un héros émerge toujours.
tags:
  - solarus
title: Révélations
---

Lorsque les ténèbres apparaissent, la lame d'un héros émerge toujours.
[Tel le reflux des marées, l'histoire se répète inlassablement.](http://www.zelda-solarus.com/revelation-marees)

Le héros vertueux révèlera le mystère de la prophétie des sept années.
[Ce n'est qu'avec sagesse que le héros découvrira sa destinée.](http://www.zelda-solarus.com/revelation-sagesse)

Depuis les ténèbres peut jaillir la perfection.
[La vérité n'est jamais cachée.](http://www.zelda-solarus.com/revelation-verite)

Sept années se sont écoulées depuis l'époque d'origine.
[La patience est la vertu du vrai héros.](http://www.zelda-solarus.com/revelation-patience)
