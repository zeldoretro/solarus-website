---
date: '2002-05-06'
excerpt: Et oui, le design est arrivé plus tôt que prévu ! J'éspère que les couleurs vous plaisent, si c'est le cas, nous les adapterons au forum pour...
tags:
- solarus
title: Déjà ?
---

Et oui, le design est arrivé plus tôt que prévu !

J'éspère que les couleurs vous plaisent, si c'est le cas, nous les adapterons au forum pour harmoniser le tout !

En tout cas, je suis déçu de l'attitude du webmaster de ZELDA ELEMENTS qui a semble-t-il changé d'avis au bout de deux semaines... C'est plutôt une bonne chose puisque j'aime bien ce design, pas vous ? Parlez-en sur le [forum](http://www.zelda-solarus.com/www.zsforums.fr.st) !
