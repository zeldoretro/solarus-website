---
date: '2018-03-12'
excerpt: Cette semaine, nous avons beaucoup avancé sur les différentes grottes de Cocolint. Cela commence vraiment à devenir très classe !
tags:
- solarus
title: Les grottes
---

Cette semaine, nous avons beaucoup avancé sur les différentes **grottes de Cocolint**. cela commence vraiment à devenir très classe !
Du coup, voici une image de l'une d'entre elles.

![](grotte-300x240.png)

N'hésitez pas à donner votre avis sur cette nouvelle image. :)
