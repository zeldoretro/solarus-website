---
date: '2002-10-23'
excerpt: Nous avons installé un système de Newsletter. Inscrivez-vous pour être régulièrement tenus au courant par e-mail des news du site. C'est le...
tags:
- solarus
title: Inscrivez-vous à la Newsletter !
---

Nous avons installé un système de Newsletter. Inscrivez-vous pour être régulièrement tenus au courant par e-mail des news du site. C'est le meilleur moyen d'être les premiers informés de ce qui se passe. Les abonnés à la Newsletter seront priviligiés car ils bénéficieront de certaines exclusivités : screenshots secrets, infos inédites... notamment à propos de Zelda Solarus 2 !

[Inscription à la Newsletter](http://www.zelda-solarus.com/newsletter.php)
