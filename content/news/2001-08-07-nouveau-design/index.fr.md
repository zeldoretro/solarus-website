---
date: '2001-08-07'
excerpt: Salut à tous ! Comme promis voici le nouveau design du site ! Plus léger, plus rapide, bref, mieux quoi ! N'hésitez pas à laisser vos...
tags:
- solarus
title: Nouveau design !
---

Salut à tous !

Comme promis voici le nouveau design du site ! Plus léger, plus rapide, bref, mieux quoi !

N'hésitez pas à laisser vos impressions sur le [forum](http://www.zelda-solarus.com/forum.php3)...
