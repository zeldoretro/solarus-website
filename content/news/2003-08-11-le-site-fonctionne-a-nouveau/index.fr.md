---
date: '2003-08-11'
excerpt: Je viens juste de rentrer de vacances et j'en ai donc profité pour rouvrir le site le plus vite possible. Une personne du site qui hébergeait nos...
tags:
- solarus
title: Le site fonctionne à nouveau
---

Je viens juste de rentrer de vacances et j'en ai donc profité pour rouvrir le site le plus vite possible. Une personne du site qui hébergeait nos images et nos téléchargements s'était amusée à tout supprimer, donc forcément plus rien ne marchait.

Mais c'est maintenant réparé. Tout est censé fonctionner à nouveau, y compris le téléchargement de la démo de Zelda : Advanced Project.

Encore toutes nos excuses pour cet incident qui souhaitons-le ne se reproduira plus...
