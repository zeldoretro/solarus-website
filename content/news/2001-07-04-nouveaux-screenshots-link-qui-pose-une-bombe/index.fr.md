---
date: '2001-07-04'
excerpt: Vous avez sûrement remarqué dans la dernière série de screenshots (voir Section&nbsp;Galerie) pas mal de nouveautés techniques. Je vous propose...
tags:
  - solarus
title: 'Nouveaux screenshots : Link qui pose une Bombe'
---

Vous avez sûrement remarqué dans la dernière série de screenshots (voir [Section Galerie](http://www.zelda-solarus.com/galerie.php3)) pas mal de nouveautés techniques. Je vous propose d'en savoir plus sur le nouveau système de gestion de Bombes. En effet, si dans la démo il suffisait d'appuyer sur Espace devant un mur fissuré pour qu'il s'ouvre, ce ne sera plus du tout le cas dans le jeu définitif. Rien de mieux que des images pour vous faire une idée :

[](solarus-ecran10.png)

[](solarus-ecran23.png)

[](solarus-ecran11.png)

[](solarus-ecran12.png)

[](solarus-ecran13.png)

- 1ère étape : Placez-vous devant le mur à exploser.
- 2ème étape : Sélectionnez les Bombes dans le menu de Pause (plus de détails sur ce nouveau menu dans une prochaine news).
- 3ème étape : Une Bombe est posée.
- 4ème étape : La Bombe explose !
- 5ème étape : Le mur est ouvert.

Vous l'avez compris, cela n'a plus rien à voir avec ce qu'on pouvait voir dans la démo !!!
