---
date: '2013-05-09'
excerpt: As promised in my previous post, I have just released new versions of Zelda Mystery of Solarus DX, Zelda Mystery of Solarus XD and Solarus! These...
tags:
- solarus
title: The German release!
---

As promised in my previous post, I have just released new versions of [Zelda Mystery of Solarus DX](http://www.solarus-games.org/downloads/download-zelda-mystery-of-solarus-dx/ "Download ZSDX"), [Zelda Mystery of Solarus XD](http://www.solarus-games.org/downloads/download-zelda-mystery-of-solarus-xd/ "Download ZSXD") and Solarus!

These updates still use the old 0.9 branch of Solarus because I haven't finished to convert both games yet to Solarus 1.0. And you have waited long enough for the German release and for important bug fixes, especially the bug of blocks that only made half moves.

Solarus 0.9.3 changes:

- The game screen size can now be set at compilation time.
- Change the savegames directory on Mac OS X.
- Improve the support of Mac OS X, Android, Pandora, Caanoo and other platforms.
- Fix the compilation with Visual C++.
- **Fix blocks making sometimes only a half move (#33).**
- Fix pixel-precise collisions not always correct (#53).
- Fix the end of target movement on slow machines (#34).
- Fix the hero being freezed when using the hookshot on bomb flowers (#119).

This should be the last release in the Solarus 0.9 branch. Our games are currently being converted to the 1.0 format.

ZSDX 1.5.2 changes:

- **German dialogs available.**
- Castle 1F: fix NPCs possibly overlapping the hero.
- West mountains cave: fix a chest possibly overlapping the hero.
- Fix various issues in French, English and Spanish dialogs.
- Fix the CMake script that creates the zip archive (#1).

ZSXD 1.5.3 changes:

- Dungeon 2: the piece of heart could be obtained very easily with the sword.
- Change the English website url in the credits dialogs.
- Fix missing lines in dialogs.
- Fix the CMake script that creates the zip archive.
- Make creepers appear earlier in the game (#1). Because creepers are fun. I like creepers.
