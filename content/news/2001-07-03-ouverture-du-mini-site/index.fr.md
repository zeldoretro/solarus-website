---
date: '2001-07-03'
excerpt: 'C''est la grande surprise que je vous préparais depuis quelque temps : l''ouverture d''un mini-site spécial Zelda : Mystery of Solarus ! Sur ce site...'
tags:
- solarus
title: Ouverture du mini-site !!
---

C'est la grande surprise que je vous préparais depuis quelque temps : l'ouverture d'un mini-site spécial Zelda : Mystery of Solarus ! Sur ce site vous retrouverez toutes les infos concernant le jeu. Grâce à la mise en place de ce mini-site, les news concernant Zelda Solarus seront bien plus fréquentes qu'avant... Revenez régulièrement !

Les premières mises à jour arriveront très bientôt, avec en particulier des explications sur les screenshots du 26 juin (voir [galerie](http://www.zelda-solarus.com/galerie.php3)).
