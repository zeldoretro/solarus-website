---
date: '2003-02-13'
excerpt: Depuis que nous avons déménagé les forums sur free.fr, ils sont très lents et fonctionnent difficilement. Alors comme notre ancien compte sur...
tags:
- solarus
title: Et les forums redéménagent !
---

Depuis que nous avons déménagé les forums sur free.fr, ils sont très lents et fonctionnent difficilement. Alors comme notre ancien compte sur consolemul existe toujours, nous venons de déménager les forums dessus. Malheureusement consolemul nous impose de la pub, alors il faudra accepter ça. Mais après tout il vaut mieux des forums rapides avec de la pub que des forums très lents, non ? :-)

[Visiter les forums](http://zelda-solarus.consolemul.com/forums)
