---
date: '2018-02-11'
excerpt: Greetings, fellows! Today we have a special screenshot which shows a sample map made with our new inner house tileset. Similar house maps will appear...
tags:
- solarus
title: 'Special screenshot: sample map of house tileset'
---

Greetings, fellows! Today we have a special screenshot which shows a sample map made with our new inner house tileset. Similar house maps will appear in our project Children of Solarus. This new tileset, still under development, includes many color variants for walls, tables, chairs, skulls, and other random and useless stuff!!! Stay tuned for incoming news.![](screenshot_house-300x225.png)
