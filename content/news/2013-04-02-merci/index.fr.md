---
date: '2013-04-02'
excerpt: L'annonce extravagante d'hier avec l'attaque de Super Tomate et le site provisoire était bien sûr un poisson d'avril ! Mais il y avait du...
tags:
- solarus
title: Merci
---

Lannonce extravagante dhier avec lattaque de Super Tomate et le site provisoire était bien sûr un poisson davril ! Mais il y avait du vrai.

Ce nouveau site na en fait rien de provisoire, il sagit de la vraie nouvelle version sur laquelle nous travaillons depuis des mois et dont je vous avais parlé dans des mises à jour précédentes. Tout nest pas terminé : il manque en particulier certains contenus de lancien site qui nont pas encore été rapatriés (quelques soluces, certains téléchargements, les mangas, lancien wiki). Tout ceci viendra peu à peu dans les jours à venir. Mais il y a aussi des nouveaux articles dont nous parlerons un peu plus tard.

Grand merci à toute léquipe qui a contribué (et contribue toujours) à cette nouvelle version du site. Merci avant tout à Binbin pour la programmation et la modernisation du site : cest lui qui a consacré le plus de temps au développement. Grand merci aussi Metallizer et à Neovyse pour le design. Et je remercie enfin Valoo, Renkineko, Morwenn et Mymy pour leurs contributions au contenu du site. Vous pouvez en savoir plus sur [léquipe du site](http://www.zelda-solarus.com/zs/equipe/ "Equipe").

Quant à Zelda : Mercuris' Chest, le projet redémarre bel et bien ! Lannulation du projet, annoncée il y a 6 ans, était en quelque sorte un très long poisson davril :) En effet, nous avons toujours secrètement espéré reprendre le projet un jour. Merci à toutes les personnes qui ont contribué au redémarrage du projet pendant un week-end de décembre 2012 pour certains et par la suite pour dautres. Par ordre alphabétique : Angenoir37, BenObiWan, Binbin, Metallizer, Miniriël, Mymy, Newlink, Renkineko, Sam101, Valoo et Yoshi04.

Bien que le jeu ne soit pas encore très avancé, les deux captures d'écran publiées hier sont réelles. Et dautres images inédites sont disponibles dans la [galerie dimages de Zelda Mercuris' Chest](http://www.zelda-solarus.com/zs/article/zmc-images/ "Images").

Dautres informations seront dévoilées très prochainement sur Mercuris' Chest.
