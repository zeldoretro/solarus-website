---
date: '2018-06-16'
excerpt: ' With the recent purchase of Github by Microsoft, the Solarus Team has decided to leave Github. We choose its alternative Gitlab, which we already...'
tags:
- solarus
title: Solarus has moved to Gitlab
---

![](gitlab-2.png)

With the recent purchase of Github by Microsoft, the Solarus Team has decided to leave Github. We choose its alternative **Gitlab**, which we already know because we used it to secretly develop our last mini-game [Mercuris Chess](http://www.solarus-games.org/games/zelda-xd2-mercuris-chess/), in April last year.

All the projects are still on Github for the moment, but in a read-only state. If you want to contribute, the code is now here on Gitlab : <https://gitlab.com/solarus-games>.

Be sure nothing will change, since Gitlab is almost identical. Development will work exactly as before.

See you soon!
