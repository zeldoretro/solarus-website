---
age: all
controls: ['keyboard', 'gamepad']
developer: ZeldoRetro
download: 'https://github.com/ZeldoRetro/defi_zeldo_chap_2/releases/download/v1.1.0/defi_zeldo_chap_2.solarus'
excerpt: After his victory on Zeldo's first challenge, Link is teleported to High-rule, where Zeldo is waiting for him to get his revenge.
genre: ['Action-RPG', 'Adventure']
id: defi_zeldo_chap_2
languages: ['en', 'fr']
license: ['GPL v3', 'CC-BY-SA 4.0', 'Proprietary (Fair use)']
maximumPlayers: 1
minimumPlayers: 1
initialReleaseDate: 2019-05-10
latestUpdateDate: 2019-05-10
screenshots: ['screen1.png', 'screen2.png', 'screen3.png', 'screen4.png', 'screen5.png', 'screen6.png', 'screen7.png', 'screen8.png', 'screen9.png', 'screen10.png', 'screen11.png', 'screen12.png']
solarusVersion: 1.6.x
sourceCode: 'https://github.com/ZeldoRetro/defi_zeldo_chap_2'
thumbnail: 'thumbnail.png'
title: "Zeldo's Challenge Ch.&nbsp;2: The Tower of Memories"
version: 1.1.0
---

### Synopsis

The game takes place just after the events of the first chapter, which was a little game with just one short dungeon beatable in 40 min, from 2017.

After having touched the Trophy of Victory won at the end of the first chapter's dungeon, Link is teleported in High-rule, a parallel world where multiple video games universes are mixed up.
Link must go to the Tower of Memories where Zeldo is waiting for him to get his revenge!

### Gaming system

The action of the game is located in Takapa's County, at the west of High-rule. Link is free to go directly to the Tower of Memories to try Zeldo's Challenge or explore this brand new world. The game is in the same vein than ALTTP but with in wackier universe than the typical _Legend of Zelda_ games, to which multiple video games characters join, and lots of references.

To complete the 100% though, it will be mandatory to explore the outside of the tower, since the tower contains items Link cannot totally explore Takapa's County without.

### Playtime

The game is rather short. You may beat it in 1 hour if you try to rush the main quest, but getting the 100% might take 3 to 6 hours, depending on your skills and exploration.

At the end of the game, a statistics screen will show you your completion and your percentage will be computed, as in Vincent Jouillat's _Zelda_ games. An achievement system, also taken from these games, has been implemented, for the most completionists players' pleasure!

### Main quest and side quests

The game has one main dungeon: the Tower of Memories, which is the main quest of the game. However, multiple side-quests and mini-dungeons are to be discovered, using familiar mechanisms from _Zelda_ games: trading quest, pieces of heart... The most dedicated players might even get into the Power Moons research, a mysterious quest that will really put to the test their exploratory skills.
