---
excerpt: Empêchez la Tribu Noire de détruire Hyrule en obtenant le pouvoir du Livre de Mudora.
---

La nouvelle quête de Link prend place quelques générations après _Twilight Princess_ and s'étend sur deux continents différents d'Hyrule. Explorez huit temples uniques et terrassez ennemis et boss pour obtenir de puissants objets qui vous aideront à atteindre votre but. Explorez de nombreuses zones d'Hyrule et aidez différentes races à résoudre leurs conflits.
