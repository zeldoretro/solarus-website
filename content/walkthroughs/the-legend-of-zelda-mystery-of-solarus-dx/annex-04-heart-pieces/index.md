---
excerpt: 'Location for all heart pieces in the game.'
tags: []
title: 'Annex: Heart Pieces'
---

## 01 - Rupees House

Do the mini-game on the right. For 10 rupees, you have to go thru a little maze and make your way thru blocks (they can be pushed several times)

[Video link : Rupees House (Piece of Heart)](http://www.youtube.com/watch?v=FanqKKhfilk#t=261s).

![Rupees House](rupees-house.png)

## 02 - Lyriann Cave

Inside the lower floor chest, after falling thru the tiny hole on the right of the starting room. You have to go thru invisible switches

[Video link : Lyriann Cave (Piece of Heart)](http://www.youtube.com/watch?v=FanqKKhfilk#t=1200s).

![Lyriann Cave](00-lyriann-cave-b1.png)

## 03 - Forest Dungeon (Level 1)

After the mid-boss, continue your way and on the fork go south. The Piece of Heart is on the cliff.

[Video link : Forest Dungeon (Piece of Heart)](http://www.youtube.com/watch?v=4Bhd-DNEN1U#t=699s).

![Forest Dungeon](a4-south-forest.png)

## 04 - Hole And Flower Cave

West of the village, fall inside the hole where two entrances are, beside the telepatic closet.

[Video link : Hole And Flower Cave (Piece of Heart)](http://www.youtube.com/watch?v=PO-issFtjgY#t=110s).

![Hole And Flower Cave](hole-and-flower-cave.png)

## 05 - Roc's Cavern (Level 2)

After the second bombflower you see, the Piece is slightly visible under the platform with two stairs. You have to lift up an invisible pot to get it.

[Video link : Roc's Cavern (Piece of Heart)](http://www.youtube.com/watch?v=PsEtixtmQ8s#t=242s).

![Roc's Cavern](02-roc-cavern-1f.png)

## 06 - North surrounding of Lyriann

North of the village (and east of Roc's Cavern, where you had to jump diagonally following leafs in the beginning of the game), after you got the super-boomerang, there is two holes you should normally jump to get it. Instead, throw you boomerang and go quickly on the right. When the boomerang will return to you, it will carry the Piece of Heart.

[Video link : North surrounding of Lyriann (Piece of Heart)](http://www.youtube.com/watch?v=b5nG2ySbX1w#t=364s).

![North surrounding of Lyriann](b3-east-lyriann.png)

## 07 - Forest Dungeon (Level 1)

After getting the bombs, blow up the wall on the first floor.

[Video link : Forest Dungeon (Piece of Heart)](http://www.youtube.com/watch?v=b5nG2ySbX1w#t=599s).

![Forest Dungeon](01-forest-dungeon-1f.png)

## 08 - Link's House

After getting the bombs, blow up the wall beside the 50 rupees chest.

[Video link : Link's House (Piece of Heart)](http://www.youtube.com/watch?v=b5nG2ySbX1w#t=680s).

![Link's House](link-house.png)

## 09 - Master Arbror's Den (Level 3)

In the black maze on the third floor. First go to the right, then when you have a 5-branch fork, take the second from the bottom, then use the button to open a path, go back to the entrance of the labyrinth, take on the left and follow the maze wall (NOT the room wall!).

[Video link : Master Arbror's Den (Piece of Heart)](http://www.youtube.com/watch?v=nr_KzjsMw4o#t=449s).

![Master Arbror's Den](03-master-arbror-den-3f.png)

## 10 - Master Arbror's Den (Level 3)

When exiting in the rooftop of the trees before getting the Master Key.

[Video link : Master Arbror's Den (Piece of Heart)](http://www.youtube.com/watch?v=nr_KzjsMw4o#t=1090s).

![Master Arbror's Den](a3-west-lyriann.png)

## 11 - Lake's shop

Blow up the wall in the backshop, the Piece is in a chest behind a stone.

[Video link : Lake's shop (Piece of Heart)](http://www.youtube.com/watch?v=abOn5rruZ3E#t=345s).

![Lake's shop](b4-lake.png)

## 12 - Beaumont's Palace (Level 4)

Under the platform in the dungeon entrance, go to the right hole and go directly on the left.

[Video link : Beaumont's Palace (Piece of Heart)](http://www.youtube.com/watch?v=PuBk5Z7JEJ8#t=80s).

![Beaumont's Palace](04-beaumont-palace-1f.png)

## 13 - Ancient Castle (Level 5)

Cannot be missed: the Piece is on the same island than the crystal switch opening the Castle gates. You have to get it to jump of the island.

[Video link : Ancient Castle (Piece of Heart)](http://www.youtube.com/watch?v=x84d8Lh24Mg#t=480s).

![Ancient Castle](a2-ancient-castle.png)

## 14 - Ancient Castle (Level 5)

After you got the Feather, jump thru the second teleport on the right and jump again thru the next one. The Piece of Heart is below on the hall.

[Video link : Ancient Castle (Piece of Heart)](http://www.youtube.com/watch?v=oeNE68E4xmk#t=929s).

![Ancient Castle](05-ancient-castle-2f.png)

## 15 - True Hero Labyrinth

On your way to get the Fire Stone of the Mountain, where the Hint-stone says "The worst is not to come", take the second stairs.

[Video link : True Hero Labyrinth (Piece of Heart)](http://www.youtube.com/watch?v=jbUnejEjXrs#t=1324s).

![True Hero Labyrinth](inferno-mountain-1f.png)

## 16 - Inferno's Daedalus (Level 6)

Come in the Big Chest room from the north and continue north.

[Video link : Inferno's Daedalus (Piece of Heart)](http://www.youtube.com/watch?v=NoAIuV9d4qA#t=840s).

![Inferno's Daedalus](06-inferno-maze-4f.png)

## 17 - Skyward Tower (Level secret)

It is in the room after the one full of stream, you need to use the hookshot.

[Video link : Skyward Tower (Piece of Heart)](http://www.youtube.com/watch?v=0mQrcTSgTt8#t=1330s).

![Skyward Tower](10-skyward-tower-4f.png)

## 18 - Sahasrahla's House

Inside the basement of the house, in a chest behind a black stone.

[Video link : Sahasrahla's House (Piece of Heart)](http://www.youtube.com/watch?v=VhKdF1L7Vak#t=380s).

![Sahasrahla's House](basement-sahasrahla.png)

## 19 - Fairy Fountain of Lyriann

Go upstair and go thru the maze.

[Video link : Fairy Fountain of Lyriann (Piece of Heart)](http://www.youtube.com/watch?v=VhKdF1L7Vak#t=660s).

![Fairy Fountain of Lyriann](lyriann-fairy-cave.png)

## 20 - Chests Cave

Open with the Stone Key, the mini-game cost you 30 rupees and allow you to get the Wooden Key (5/16 chances) but also a Piece of Heart (3/16 chances).

[Video link : Chests Cave (Piece of Heart)](http://www.youtube.com/watch?v=VhKdF1L7Vak#t=1020s).

## 21 - North-east surrounding of Lyriann

Behind the door opened by the Wooden Key, open all the chests without falling.

[Video link : North-east surrounding of Lyriann (Piece of Heart)](http://www.youtube.com/watch?v=VhKdF1L7Vak#t=1110s).

![North-east surrounding of Lyriann](waterfall-cave.png)

## 22 - Surrounding of the Lake

After jumping of the village waterfall, take the path on the right and get out on the cliff.

[Video link : Surrounding of the Lake (Piece of Heart)](http://www.youtube.com/watch?v=VhKdF1L7Vak#t=1260s).

![Surrounding of the Lake](b4-lake.png)

## 23 - Bazaar

Buy it for 499 rupees.

[Video link : Bazaar (Piece of Heart)](http://www.youtube.com/watch?v=VhKdF1L7Vak#t=1395s).

## 24 - Crystal Temple (Level 7)

In the useless pipe, on the north-west room.

[Video link : Crystal Temple (Piece of Heart)](http://www.youtube.com/watch?v=Q1hjSJMHr0Y#t=195s).

![Crystal Temple](07-crystal-temple-1f.png)

## 25 - Mount Terror

From west entrance of the mountains (the one you take to go to the Crystal Temple, after the maze in the dark), use the Cane of Somaria in the first room to go to the right with the hookshot.

[Video link : Mount Terror (Piece of Heart)](http://www.youtube.com/watch?v=yMUZTDv7_Vs#t=75s).

![Mount Terror](west-mount-cave-1f.png)

## 26 - Twin Caves

A chest accessible only thanks to a Somaria block.

[Video link : Twin Caves (Piece of Heart)](http://www.youtube.com/watch?v=yMUZTDv7_Vs#t=340s).

![Twin Caves](twin-caves.png)

## 27 - South River

Since the previous Piece of Heart, enter the west cave, use the Cane of Somaria to go to the exit. The Piece of Heart is on the other end of the cliff, in front of the island where you found the Ice Key.

[Video link : South River (Piece of Heart)](http://www.youtube.com/watch?v=yMUZTDv7_Vs#t=410s).

![South River](b4-lake.png)

## 28 - Mount Terror

At the base of the plant grown to access Level 8.

[Video link : Mount Terror (Piece of Heart)](http://www.youtube.com/watch?v=yMUZTDv7_Vs#t=800s).

![Mount Terror](b1-east-mount-terror.png)

## 29 - Mount Terror

After reverting the waterfall with the Mystic Mirror, jump from the cliff to enter the first possible cave, go thru several spikes to get the Piece of Heart (and fall in the hole to get the last Magic Bottle).

[Video link : Mount Terror (Piece of Heart)](http://www.youtube.com/watch?v=yMUZTDv7_Vs#t=940s).

![Mount Terror](west-mount-cave-3f.png)

## 30 - Rock Peaks Dungeon (Level 8)

In the room where the Big Chest is (left of the Boss door), the wall on the left can be blown up.

[Video link : Rock Peaks Dungeon (Piece of Heart)](http://www.youtube.com/watch?v=4HEzzgsQepM#t=1841s).

![Rock Peaks Dungeon](08-rock-peaks-dungeon-1f.png)

## 31 - Mount Terror

Use a Somaria block to go to the room in the dark with two gibdos and two lizards, go to the right, fall in the hole, get out of the mountain and follow down the bridge. There is a cave with six lasers (you can get thru without the Mirror Shield). Piece of Heart is at the end.

[Video link : Mount Terror (Piece of Heart)](http://www.youtube.com/watch?v=Z7CwjZpo2b8#t=330s).

![Mount Terror](laser-cave.png)

## 32 - Shrine of Memories (Level 9)

On the second floor, get out on the east exit. The Piece of Heart is outside, between the two exits.

[Video link : Shrine of Memories (Piece of Heart)](http://www.youtube.com/watch?v=qhiBvDKNSSI#t=1200s).

![Shrine of Memories](a4-south-forest.png)
