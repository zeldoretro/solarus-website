---
title: Community
excerpt: Join the Solarus community in the forums and Discord.
type: singles
layout: community
tags: [community, irc, forum, forums, chat]
---
